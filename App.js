/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component, useState, useEffect } from "react";
import { WebView } from "react-native-webview";
import {
  ActivityIndicator,
  PermissionsAndroid,
  SafeAreaView,
} from "react-native";

const App: () => React$Node = () => {
  const [hasLocationPermission, setHasLocationPermission] = useState(false);
  const [userPosition, setUserPosition] = useState(false);

  verifyLocationPermission();

  async function verifyLocationPermission() {
    try {
      if (Platform.OS === "android") {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          setHasLocationPermission(true);
        } else {
          setHasLocationPermission(false);
        }
      } else {
        setHasLocationPermission(true);
      }
    } catch (err) {
      console.log(err);
    }
  }

  const renderLoading = () => (
    <ActivityIndicator
      style={{ flex: 1 }}
      animating
      color="#222026"
      size="large"
    />
  );

  return (
    <>
      <SafeAreaView style={{ flex: 0, backgroundColor: "#202125" }} />
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          style={{ flex: 1 }}
          startInLoadingState={true}
          scalesPageToFit={true}
          renderLoading={renderLoading}
          source={{ uri: "https://portaldospets.com/teste/" }}
        />
      </SafeAreaView>
    </>
  );
};

export default App;
